function GetQueryString(){
	if(1 < window.location.search.length){
		var query = window.location.search.substring(1);
		var parameters = query.split('&');
		var result = new Object();
		for(var i=0 ; i<parameters.length ; i++ ){
			var element = parameters[i].split( '=' );
			var paramName  = decodeURIComponent(element[0]);
			var paramValue = decodeURIComponent(element[1]);
			result[paramName] = decodeURIComponent(paramValue);
		}
		return result;
	}
	return null;
}

function itemOrder(money) {
	document.forms[0].credit.value = "";

	var s = "クレジット決算";
	for(var i=0 ; i<document.forms[0].お支払い方法.length ; i++){
		if(document.forms[0].お支払い方法[i].checked){
			var payment = document.forms[0].お支払い方法[i].value.substring(0, s.length);
			if(payment != s){
				return true;
			}
		}
	}

	// 送料加算
	var url = location.href;
	var name = url.substring(url.lastIndexOf('/')+1, url.length);
	var obj = document.forms[0].都道府県;
	if(obj.options[obj.selectedIndex].text == "北海道"){
		;
	} else if(obj.options[obj.selectedIndex].text == "沖縄県"){
		;
	}

	// オールステンレスハンドリフト
	if(name == 'products04_order.html'){
		money += 4000;

	// 高性能ハンドリフト
	} else if(name == 'products01_order.html'){
		money += 3000;

	// 超低床ハンドリフト
	} else if(name == 'products02_order.html'){
		money += 3000;

	// フォークリフト
	} else if(name == 'products08_order.html'){
		money += 8500;

	// ハイアップハンドリフト
	} else if(name == 'products03_order.html'){
		money += 5000;
	}

	document.forms[0].credit.value = money;
	return true;
}

function optionOrder(money) {
	document.forms[0].credit.value = "";
	var params = GetQueryString();
	if('credit' in params){
		document.forms[0].credit.value = money;	// 価格
	}
	return true;
}

