<?php
/**
 * The template part for normal loop
 *
 */
?>

<?php if ( have_posts() ) : ?>
	<?php 
		while( have_posts() ) :
			the_post();
	?>
        		<?php 
        			echo wpautop( get_the_content(), false );
        		?>
	<?php 
		endwhile ;
	?>
<?php endif ; ?>