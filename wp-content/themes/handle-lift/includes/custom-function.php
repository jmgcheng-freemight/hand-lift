 <?php
 
 //check if page is using order form template
function is_order_form($id){
	$template = get_page_template_slug( $post->ID );
	$template_array = explode("/", $template);
	if($template_array[1] == 'order-form.php')return true; else return false;
}

//specific style for order form only
function order_style(){
	$style = '<style>
.hissu {
	background: red;
	border: 1px solid red;
	border-radius: 8px;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	color: #FFFFFF;
	width: 35px;
	padding: 2px 0;
	text-align:center;
	font-size:10px;
	float:right;
}

.backimg{
	background-image:url("'.get_template_directory_uri().'/image/list/handlift.png");
	background-repeat:no-repeat;
	background-position:right bottom -5px;
	background-size:160px;
}

.backalp{
	background: rgba(255, 255, 255, 0.7);
}

input{
	background-color:#CCCCCC;
}

input:focus {
	background-color:#FFFFFF;
}

#impt_back{
	background-image: url("'.get_template_directory_uri().'/dream/img/contact_btn_background.png");
	background-repeat: no-repeat;
	background-position: center;
}
#impt{
	background-color: transparent;
}
.fixedWidget {
	position:relative;
	position: fixed;
	top: 0px;
}
</style>';
	 return $style;
 }
 
apply_filters ( 'wp_mail_from', 'staff@freemight.com' );
apply_filters ( 'wp_mail_from_name', 'Hand-lift'); //if doesn't work edit wp-includes/pluggable.php line 312

function send_order() {
	global $wpdb;
	
	date_default_timezone_set('Asia/Tokyo');
	
	$order = $_POST['order'];
	$pid = $order['product_id'];
	$now = date('Y/m/d H:i:s');
	$y = date('Y');
	$m = date('m');
	$d = date('d');
	$h = date('h');
	$i = date('i');
	$s = date('s');
	//generate order number
	$myrows = $wpdb->get_results( "SELECT order_id FROM order_ctr ORDER BY order_id DESC LIMIT 1" );
	if($myrows == null){
		$order_no = 'HAND0000001';
		$insert_data = array( 
				'order_number' => $order_no, 
				'order_log' => $now 
				);
		$wpdb->insert('order_ctr', $insert_data,'' );
	}else{
		$order_no = generate_order_no($myrows[0]);
		$insert_data = array( 
				'order_number' => $order_no, 
				'order_log' => $now 
				);
		$wpdb->insert('order_ctr', $insert_data,'' );
	}
	
	//apply email title filter 
	$email_title = 'NE受注メール（'.$order_no.')';
	
	//construct email and order post content
	$total = $order['item_amount'] + $order['shipping_charge'];
	$content='
			注文コード： '.$order_no.'
			注文日時(order date)：'.$y.'年'.$m.'月'.$d.'日 '.$h.'時'.$i.'分'.$s.'秒 (TOKYO Time)
			■注文者の情報
			氏名：'.$order['surname'].' '.$order['name'].'
			氏名（フリガナ）：'.$order['say'].' '.$order['mei'].'
			郵便番号：'.$order['postal_code'].'
			住所：'.$order['address'].' '.$order['building_name'].'
			電話番号：'.$order['telephone_number'].'
			Ｅメールアドレス：'.$order['email'].'
			■支払方法
			支払方法：'.$order['payment_method'].'
			■注文内容
			------------------------------------------------------------
			商品番号：'.$order['product_code'].'
			注文商品名： '.$order['product_name'].'
			商品オプション：
			単価：￥'.number_format($order['item_amount']).'
			数量：1
			小計：￥'.number_format($order['item_amount']).'
			------------------------------------------------------------
			商品合計：￥'.number_format($order['item_amount']).'
			税金：￥0
			送料：￥'.number_format($order['shipping_charge']).'
			手数料：￥0
			その他費用：￥0
			------------------------------------------------------------
			合計金額(税込)：￥'.number_format($total).'
			------------------------------------------------------------
			■届け先の情報
			[送付先1]
			　送付先1氏名：'.$order['surname'].' '.$order['name'].'
			　送付先1氏名（フリガナ）：'.$order['say'].' '.$order['mei'].'
			　送付先1郵便番号：'.$order['postal_code'].'
			　送付先1住所：'.$order['address'].' '.$order['building_name'].'
			　送付先1電話番号：'.$order['telephone_number'].'
			　送付先1お届け方法：'.$order['delivery_method'].'
			■通信欄
			テスト受注です。配送しないで下さい。';
	
	//create order post 
	$metas = array( 'order_no' => $order_no, 'product_id' => $pid);
	
	$postarr = array(
				'comment_status'	=>	'closed',
				'ping_status'		=>	'closed',
				'post_content'		=> $content,
				'post_author'		=>	1,
				'post_name'			=>	$slug,
				'post_title'		=>	'Order on '.$now.' (Tokyo Time)',
				'post_status'		=>	'publish',
				'post_type'			=>	'orders',
				'meta_input' 		=> $metas
				);
				
	wp_insert_post ( $postarr, false );

	//send email
	$to = array($order['email']);
	$subject = $email_title;
	$headers = array("From:Hand-lift <staff@freemight.com>");
	//$headers = 'Order Details';
	wp_mail( $to, $subject, $content, $headers);
	
	//update available stock and sold units
	$av_stock = get_post_meta( $pid, 'av_stock', '' );
	$av_stock = $av_stock[0] - 1;
	update_post_meta($pid,'av_stock',$av_stock); 

	$sold_unit = get_post_meta( $pid, 'sold_units', '' );
	if($sold_unit):
		$sold_unit = $sold_unit[0] + 1; 
		update_post_meta($pid,'sold_units',$sold_unit);
	else:
		$sold_unit = 1; 
		update_post_meta($pid,'sold_units',$sold_unit);
	endif;

	echo json_encode(array('order_no' => $order_no));
	wp_die();
}
// ajax send order to email
add_action( 'wp_ajax_nopriv_send_order', 'send_order' );
add_action( 'wp_ajax_send_order', 'send_order' );

function generate_order_no($row){
	
	$id = $row->order_id;
	$i = $id + 1;
	$pre = 'HAND';
	if($id <= 9) $order_no = $pre.'000000'.$i;
	elseif($id <= 99) $order_no = $pre.'00000'.$i;
	elseif($id <= 999) $order_no = $pre.'0000'.$i;
	elseif($id <= 9999) $order_no = $pre.'000'.$i;
	elseif($id <= 99999) $order_no = $pre.'00'.$i;
	elseif($id <= 999999) $order_no = $pre.'0'.$i;
	elseif($id <= 9999999) $order_no = $pre.$i;
	
	return $order_no;
}