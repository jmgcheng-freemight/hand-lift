<?php 
	get_header();
	
?>


			        <!--+++.container+++-->
			        <div class="container cf">


						<?php 
							get_sidebar(); 
						?>


			            <!--++.main/++-->
			            <div class="main">

			                <?php if ( have_posts() ) : ?>
			                	<?php 
			                		while( have_posts() ) :
			                			the_post();
			                	?>

					                		<?php
												if($_SERVER['HTTP_HOST'] == 'hl.dev')
													$content = str_replace('http://freemight.com/demo/hand-lift', 'http://hl.dev', get_the_content());
					                			else
													$content = str_replace('http://hl.dev', 'http://freemight.com/demo/hand-lift', get_the_content());
													
												echo wpautop( $content, false ); 
					                		?>

			                	<?php 
			                		endwhile ;
			                	?>
			            	<?php endif ; ?>

			            </div>
			            <!--/.main-->
			            <!--++/.main++-->


			        </div>
			        <!--+++/.container+++-->


<?php 

	get_footer(); 
?>