	            <!--++.side/++-->
	            <div class="side display-pc">
	            	<nav class="side-nav">
	            		<h3 class="side-nav-h">
	            			メインメニュー
	            		</h3>
	            		<ul>
	            			<li>
	            				<a href="<?php bloginfo ('url'); ?>">ホーム</a>
	            			</li>
	            			<li>
	            				<a href="<?php bloginfo ('url'); ?>/list">取扱商品一覧</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/guide">納期・送料・支払いについて</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/hosyo">安心保証について</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/reason">当店が選ばれる理由</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/announce">格安でご提供できる理由</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/large-order">業者様・大口注文の方へ</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/download">商品カタログ</a>
	                        </li>
	            		</ul>
	            	</nav>
	            	<nav class="side-nav">
	            		<h3 class="side-nav-h">
	            			商品カテゴリ
	            		</h3>
	            		<ul>
	            			<li>
	            				<a href="<?php bloginfo ('url'); ?>/products/product01">高性能ハンドリフト</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/products/product02">超低床ハンドリフト</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/products/product03">ハイアップハンドリフト</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/products/product04">オールステンレスハンドリフト</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/products/product08">フォークリフト</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/products/product05">パレットスケール (計量可能)</a>
	                        </li>
	            		</ul>
	            	</nav>
	            	<nav class="side-nav">
	            		<h3 class="side-nav-h">
	            			会社概要
	            		</h3>
	            		<ul>
	            			<li>
	            				<a href="<?php bloginfo ('url'); ?>/staff">スタッフ紹介</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/company">会社概要</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/useguide">ご利用ガイド</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/toiawase">お問合せ（メール）</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/tel">お問合せ（電話）</a>
	                        </li>
	                        <li>
	                        	<a href="<?php bloginfo ('url'); ?>/sitemap">サイトマップ</a>
	                        </li>
	            		</ul>
	            	</nav>

	                <div class="side_bloc">
	                    <script type="text/javascript" src="https://gsl-co2.com/mark/?siteh=http://maki-wari.com/&i=6"></script>
	                    <p>
	                        <a href="http://www.challenge25.go.jp/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/c25_logo_1.gif" border="0" alt="チャレンジ25" />
	                        </a>
	                    </p>
	                </div>


	            </div>
	            <!--/.side-->
	            <!--++/.side++-->