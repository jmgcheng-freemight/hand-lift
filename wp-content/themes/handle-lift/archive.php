<?php 
	get_header();
?>

	        <!--+++.container+++-->
	        <div class="container cf">


				<?php 
					get_sidebar(); 
				?>

	            <!--++.main/++-->
	            <div class="main">

	            	<?php 
	            		$s_slug = get_post_type();
						if( isset($s_slug) && !empty($s_slug) )
						{
							echo '<div style="display:none;">here hermitarhive ' . $s_slug . '</div>';
							$a_args = array(
								'post_type' => 'page',
								'pagename' => $s_slug
							);
							$o_custom_query = new WP_Query( $a_args );
							if ( $o_custom_query->have_posts() ) 
							{
								while ( $o_custom_query->have_posts() ) 
								{
									$o_custom_query->the_post();
									echo wpautop( get_the_content(), false );
								}
							} 
							else 
							{
								get_template_part( 'template-parts/loop', 'normal' );
							}
							wp_reset_postdata();
						}
						else
						{
							get_template_part( 'template-parts/loop', 'normal' );
						}
	            	?>

	            </div>
	            <!--/.main-->
	            <!--++/.main++-->


	        </div>
	        <!--+++/.container+++-->


<?php 
	get_footer(); 
?>