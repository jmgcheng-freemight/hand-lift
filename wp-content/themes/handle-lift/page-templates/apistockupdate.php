<?php
/**
 * Template Name: apistockupdate
 *
 * @package Hand-lift 1.0
 * @since Hand-lift 1.0
 */
 ?>

<?php
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Allow-Headers: origin, content-type, accept');
	

	/**/
	$i_post_id = 0;
	$s_post_permalink = '';
	$a_result = array();
	

	/**/
	if( 	isset($_GET['StoreAccount']) && !empty($_GET['StoreAccount']) 
		&& 	isset($_GET['Code']) && !empty($_GET['Code']) 
		&& 	isset($_GET['Stock']) && !empty($_GET['Stock']) 
	)
	{}
	else
	{
		wp_redirect( home_url() ); 
		exit();
	}


	/**/
	$a_args = array(
		'post_type'  		=> 'products',
		'meta_key'   		=> 'product_number',
		'posts_per_page' 	=> 1,
		'meta_query' => array(
			array(
				'key'     => 'product_number',
				'value'   => $_GET['Code']
			),
		),
	);
	$o_query = new WP_Query( $a_args );
	if ( $o_query->have_posts() ) 
	{
			$o_query->the_post();
			$i_post_id = get_the_ID();
			$s_post_permalink = get_permalink($i_post_id);
	}
	else
	{
		wp_redirect( home_url() ); 
		exit();	
	}
	wp_reset_postdata();


	/**/
	if( isset($i_post_id) && !empty($i_post_id) )
	{
		/**/
		update_post_meta($i_post_id, 'av_stock', $_GET['Stock']);

		/*
		wp_redirect( $s_post_permalink );
		*/

		echo 'updated';
		exit();
	}
	else
	{
		wp_redirect( home_url() );
		exit();	
	}
?>