<?php
/**
 * Template Name: Thank You
 *
 * @package Hand-lift 1.0
 * @since Hand-lift 1.0
 */
 ?>
 
<?php 
	get_header();
	
?>
<div class="container cf">


			<?php 
				get_sidebar(); 
			?>
<!--++.main/++-->
<div class="main">
<!--++.sec/++-->
<h3 class="ti">ご注文完了</h3>
<div id="toiawase_thank" class="sec cf">
 
 <p class="al_center"><strong>ご用命ありがとうございました。</strong></p>
 <p class="al_center">翌営業日内に、改めてお返事をさせて頂きます。<br />今しばらく、お待ちくださいませ。</p>
 
 <p class="al_center">何か、ご不明な点やご質問などございましたら気兼ねなくお問合せくださいませ。 <br />どうぞ、よろしくお願いいたします。</p>
 
 <p class="al_center"><a href="../tel.html"><img src="<?php echo get_template_directory_uri();?>/common/img/tel_banner_off.gif" alt="お電話でのお見積り、ご相談はこちら" /></a></p>  
</div>
<!--++/.sec++-->


</div><!--/.main-->
<!--++/.main++-->
<?php 

	get_footer(); 
?>