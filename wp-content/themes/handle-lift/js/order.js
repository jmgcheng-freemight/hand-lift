$("#fade").modal({
  fadeDuration: 100
});

$(document).ready(function() {
	
	function order_details() {
		
		this.product_id = $('.pid').val();
		this.product_code = $('.pcode').val();
		this.product_name = $('.pname').val();
		
		this.surname = $('.surname').val();
		this.name =$('.name').val();
		this.say =$('.say').val();
		this.mei =$('.mei').val();
		this.postal_code =$('.postal-1').val() +'-'+ $('.postal-2').val(); 
		this.pref =$('#prefecture').val();
		this.address =$('.address').val(); 
		this.building_name=$('.building-name').val();
		this.telephone_number=$('.tel-1').val() +'-'+ $('.tel-2').val() +'-'+ $('.tel-3').val(); 
		this.email=$('.e-mail').val();
		
		this.item_amount=$('.amount').val();
		this.payment_method = $('.payment:checked').val();
		this.shipping_charge=$('.shipping').val();
		this.delivery_method=$('.delivery').val();
		
	}
	
	$( document ).on( 'click', '.submit_order', function(event) {
		
		var order = new order_details();
		
		$('.pay-method').text(order.payment_method);
		$('.txt-surname').text(order.surname);
		$('.txt-name').text(order.name);
		$('.txt-say').text(order.say);
		$('.txt-mei').text(order.mei);
		$('.txt-postal-code').text(order.postal_code);
		$('.txt-prefectures').text(order.pref);
		$('.txt-address').text(order.address);
		$('.txt-building-name').text(order.building_name);
		$('.txt-telephone-number').text(order.telephone_number);
		$('.txt-email').text(order.email);
		
	});
	
	$( document ).on( 'click', '.btn-final-submit', function(event) {
		
		var order = new order_details();
		
		$.ajax({
			url: ajaxurl,
			type: "POST",
			data: {
				'action': 'send_order',
				'order': order
			},
			success:function(response) {
				console.log(response);
				
				$('.jquery-modal').hide();
				window.location.replace("../thank-you-message/");
				
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
		
	});
	
});