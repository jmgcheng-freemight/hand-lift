;/*****************************************************************
 * Japanese language file for jquery.validationEngine.js (ver2.0)
 *
 * Transrator: tomotomo ( Tomoyuki SUGITA )
 * http://tomotomoSnippet.blogspot.com/
 * Licenced under the MIT Licence
 *******************************************************************/
(function($){
    $.fn.validationEngineLanguage = function(){
    };
    $.validationEngineLanguage = {
        newLang: function(){
            $.validationEngineLanguage.allRules = {
	            
                "checkSeiKatakana": {
                    "regex": "^[ƒA-ƒ“JKƒ@-ƒHƒƒ-ƒ‡[uvA]+$",
                    "alertText": "<span class='close'>~</span>* uƒZƒCv‚ÍƒJƒ^ƒJƒi‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "checkMeiKatakana": {
                    "regex": "^[ƒA-ƒ“JKƒ@-ƒHƒƒ-ƒ‡[uvA]+$",
                    "alertText": "<span class='close'>~</span>* uƒƒCv‚ÍƒJƒ^ƒJƒi‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
	            
                "requiredSeiMei": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ‚¨–¼‘O‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredSei": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ©‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredMei": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* –¼‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredSeiKana": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ƒZƒC‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredMeiKana": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ƒƒC‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredZip": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* —X•Ö”Ô†‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredPref": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* “s“¹•{Œ§‚ð‘I‘ð‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredAddress": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ZŠ‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredTel": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* “d˜b”Ô†‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "requiredMail": {
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ƒ[ƒ‹ƒAƒhƒŒƒX‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "required": { 
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* •K{€–Ú‚Å‚·"
                },
                "requiredInFunction": { 
                    "func": function(field, rules, i, options){
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* Field must equal test"
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": "•¶ŽšˆÈã‚É‚µ‚Ä‚­‚¾‚³‚¢"
                },
		"groupRequired": {
                    "regex": "none",
                    "alertText": "* You must fill one of the following fields"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": "•¶ŽšˆÈ‰º‚É‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": " ˆÈã‚Ì”’l‚É‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "max": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": " ˆÈ‰º‚Ì”’l‚É‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "past": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": " ‚æ‚è‰ß‹Ž‚Ì“ú•t‚É‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "future": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": " ‚æ‚èÅ‹ß‚Ì“ú•t‚É‚µ‚Ä‚­‚¾‚³‚¢"
                },	
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* ƒ`ƒFƒbƒN‚µ‚·‚¬‚Å‚·"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* ",
                    "alertText2": "‚ÂˆÈãƒ`ƒFƒbƒN‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* “ü—Í‚³‚ê‚½’l‚ªˆê’v‚µ‚Ü‚¹‚ñ"
                },
                "creditCard": {
                    "regex": "none",
                    "alertText": "* –³Œø‚ÈƒNƒŒƒWƒbƒgƒJ[ƒh”Ô†"
                },
                "name1": { 
                    "regex": "none",
                    "alertText": "<span class='close'>~</span>* ©–¼‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "zip": {
                    // credit: 2015-07-08 MEGALABO
                    "regex": /^\d{3}-\d{4}$|^\d{7}$/,
                    "alertText": "<span class='close'>~</span>* —X•Ö”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "zip1": {
                    // credit: 2015-07-08 MEGALABO
//                     "regex": /^\d{3}$|^\d{3}$/,
                    "regex": /^[0-9]{3}$/,
                    "alertText": "<span class='close'>~</span>* —X•Ö”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "zip2": {
                    // credit: 2015-07-08 MEGALABO
//                     "regex": /^\d{3}$|^\d{3}$/,
                    "regex": /^[0-9]{4}$/,
                    "alertText": "<span class='close'>~</span>* —X•Ö”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "phone": {
                    "regex": /^(\d{2,4}-\d{2,4}-\d{4}?)$/,
                    "alertText": "<span class='close'>~</span>* “d˜b”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "phone1": {
//                     "regex": /^(\d{2,4}?)$/,
                    "regex": /^([0-9‚O-‚X]{2,4}?)$/,
                    "alertText": "<span class='close'>~</span>* “d˜b”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "phone2": {
//                     "regex": /^(\d{2,4}?)$/,
                    "regex": /^([0-9‚O-‚X]{2,4}?)$/,
                    "alertText": "<span class='close'>~</span>* “d˜b”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "phone3": {
//                     "regex": /^(\d{2,4}?)$/,
                    "regex": /^([0-9‚O-‚X]{2,4}?)$/,
                    "alertText": "<span class='close'>~</span>* “d˜b”Ô†‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "email": {
                    // Shamelessly lifted from Scott Gonzalez via the Bassistance Validation plugin http://projects.scottsplayground.com/email_address_validation/
                    "regex": /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
                    "alertText": "<span class='close'>~</span>* ƒ[ƒ‹ƒAƒhƒŒƒX‚ª³‚µ‚­‚È‚¢‰Â”\«‚ª‚ ‚è‚Ü‚·B"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* ®”‚ð”¼Šp‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* ”’l‚ð”¼Šp‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "date": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                    "alertText": "* “ú•t‚Í”¼Šp‚Å YYYY-MM-DD ‚ÌŒ`Ž®‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "ipv4": {
                	"regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* IPƒAƒhƒŒƒX‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    "alertText": "* URL‚ª³‚µ‚­‚ ‚è‚Ü‚¹‚ñ"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* ”¼Šp”Žš‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* ”¼ŠpƒAƒ‹ƒtƒ@ƒxƒbƒg‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* ”¼Šp‰p”‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                },
                // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
                "ajaxUserCall": {
                    "url": "ajaxValidateFieldUser",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    "alertText": "* This user is already taken",
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* This name is already taken",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This name is available",
                    // speaks by itself
                    "alertTextLoad": "* Validating, please wait"
                },
                "validate2fields": {
                    "alertText": "* wHELLOx‚Æ“ü—Í‚µ‚Ä‚­‚¾‚³‚¢"
                }
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


    
