/**
 * inputã‚°ãƒ¬ãƒ¼ã‚¢ã‚¦ãƒˆç”¨
 */
$(function(){

  $('form#contact-form input[type=text],form#contact-form select,
     form#contact-form textarea').each(function() {
             var $self = $(this);
             $self.css({border:'1px solid #999999'});
             if (!$self.val() && $self.attr('id') != 'AccountCompanyName') {
                 $self.css({backgroundColor:'#D4D4D4'})
                     .focus(function() {
                                $(this).css({backgroundColor:'#FFFFFF'});
                            })
                     .blur(function() {
                               var $self = $(this);
                               if ($self.val()) {
                                   $self.css({backgroundColor:'#FFFFFF'});
                               } else {
                                   $self.css({backgroundColor:'#D4D4D4'});
                               }
                               if ($self.attr('id') == 'AccountZip2' || $self.attr('id') == 'AccountAgentZip2') {
                                   fireChangePrefecture();
                               }
                           })
                     .change(function() {
                                 var $self = $(this);
                                 if ($self.val()) {
                                     $self.css({backgroundColor:'#FFFFFF'});
                                 } else {
                                     $self.css({backgroundColor:'#D4D4D4'});
                                 }
                                 if ($self.attr('id') == 'AccountZip2' || $self.attr('id') == 'AccountAgentZip2') {
                                     fireChangePrefecture();
                                 }
                             });
             }
             function fireChangePrefecture() {
                 $('#AccountPrefecture').change();
                 $('#AccountAgentPrefecture').change();
             }
         });
  });
