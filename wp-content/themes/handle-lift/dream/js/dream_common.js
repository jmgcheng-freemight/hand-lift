var ureru_efo_common = {
    initialize: function() {
    },

    // ƒoƒŠƒf[ƒVƒ‡ƒ“ŽÀs
    valid: function(self, key, name, extraFunctionList) {
    },
    /*
    // Žd—l•ÏX‚É‚æ‚èíœ
    valid: function(self, key, name, extraFunctionList) {
        var validRule = efoValidationData[key];

        if (self.length == 0) {
            return '';
        }
        
        if (name === undefined || name == null) {
            name = validRule.name
        }

        if (validRule.flg == '0') {
            // ƒoƒŠƒf[ƒVƒ‡ƒ“‚È‚µ
            return '';
        }

        if (validRule.required == '1') {
            // •K{€–Ú
            if (ureru_efo_common.required(self) == false) {
                return name + '‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢';
            }
        }

        if (validRule.limit != '0') {
            // •¶Žš”§ŒÀ‚ ‚è
            if (ureru_efo_common.maxLength(self, validRule.limit) == false) {
                return validRule.limit + '•¶ŽšˆÈ“à‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢'
            }
        }

        if (extraFunctionList !== undefined) {
            var len = extraFunctionList.length;
            for (var i = 0; i < len; i ++) {
                var retMessage = extraFunctionList[i](self);
                if (retMessage != "") {
                    return name + retMessage;
                }
            }
        }

        return '';
    },*/

    // •K{ƒ`ƒFƒbƒN
    isRequired: function(self, key) {
        var validRule = efoValidationData[key];

        if (validRule.required == '0') {
            // •K{‚Å‚Í‚È‚¢
            return true;
        }

        var val = '';
        val = self.multiVal();

        if (val != '' && val.match(/^(\s|@)+$/) == null) {
            return true;
        } else {
            return false;
        }
    },

    // Å‘åŒ…”ƒ`ƒFƒbƒN(ƒ`ƒFƒbƒN‘ÎÛ‚Íinput[type=text] or textarea)
    isMaxLength: function(self, key) {
        var validRule = efoValidationData[key];
        var tagName = self.get(0).tagName.toLowerCase();
        var type = self.attr('type');
        var val = '';

        if (validRule.limit == '0') {
            // Œ…”ƒ`ƒFƒbƒN‚ð‚µ‚È‚¢
            return true;
        }

        if (tagName != 'input' && tagName != 'textarea') {
            // ƒ`ƒFƒbƒN‘ÎÛ‚¶‚á‚È‚¢(select)
            return true;
        }
        if (tagName == 'input' && (type != 'text' && type != 'email' && type != 'tel')) {
            // ƒ`ƒFƒbƒN‘ÎÛ‚¶‚á‚È‚¢(check or radio)
            return true;
        }

        val = self.val();
        val = val.replace(/\r\n/g, "").replace(/\r/g, "").replace(/\n/g, "");
        if (val.length > validRule.limit) {
            return false;
        } else {
            return true;
        }
    },

    // ‘SŠp‚Ì‚Ýƒ`ƒFƒbƒN
    isZenkakuOnly: function(self) {
        var val = self.val();

        if (val == '') {
            // ‹ó‚Ìê‡‚Í‰½‚à‚µ‚È‚¢
            return true;
        }

        if (val.match(/(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F])|[\x20-\x7E]/) == null) {
            return true;
        } else {
            return false;
        }
    },

    // ‚Ð‚ç‚ª‚È‚Ì‚Ýƒ`ƒFƒbƒN
    isHiraganaOnly: function(self) {
        // @todo: ƒT[ƒo‘¤‚Æ“¯‚¶‚à‚Ì‚ªŽg‚¦‚È‚¢
        var val = self.val();

        if (val == '') {
            // ‹ó‚Ìê‡‚Í‰½‚à‚µ‚È‚¢
            return true;
        }

        if (val.match(/[^‚Ÿ-‚ñ|^[|^J]/g) == null) {
            return true;
        } else {
            return false;
        }
    },

    // ”’lƒ`ƒFƒbƒN
    isNumeric: function(self) {
        var val = self.val();

        if (val == '') {
            // ‹ó‚Ìê‡‚Í‰½‚à‚µ‚È‚¢
            return true;
        }

        if (val.match(/^[0-9]+$/) != null) {
            return true;
        } else {
            return false;
        }
    },

    // —X•Ö”Ô†‚Ìƒ`ƒFƒbƒN
    isZip: function() {
        var zip1 = jQuery('.ureru_efo_zip1', ureru_efo.landingFormObject).val();
        var zip2 = jQuery('.ureru_efo_zip2', ureru_efo.landingFormObject).val();

        if (zip1 == '' || zip2 == '') {
            return true;
        }

        if (zip1.match(/^[0-9]{3}$/) != null && zip2.match(/^[0-9]{4}$/) != null) {
            return true;
        } else {
            return false;
        }
    },

    // “d˜b”Ô†‚Ìƒ`ƒFƒbƒN
    // “ü—Í‚ª’†“r”¼’[‚Èê‡‚Ínull‚ª‹A‚è‚Ü‚·
    isTelNo: function(key1, key2, key3) {
        var tel1 = jQuery(key1, ureru_efo.landingFormObject).val();
        var tel2 = jQuery(key2, ureru_efo.landingFormObject).val();
        var tel3 = jQuery(key3, ureru_efo.landingFormObject).val();

        if (tel1 == '' || tel2 == '' || tel3 == '') {
            // ‘S•”‚»‚ë‚í‚È‚¢‚Æƒ_ƒB
            return null;
        }

        // ”’lˆÈŠO‚ª“ü‚Á‚½ê‡‚ànull‚ð•Ô‚·
        if (tel1.match(/^[0-9]+$/) == null || tel2.match(/^[0-9]+$/) == null || tel3.match(/^[0-9]+$/) == null) {
            return null;
        }

        var tel = tel1 + tel2 + tel3;
        if (tel.length <= 11) {
            return true;
        } else {
            return false;
        }
    },
    
    // ƒ[ƒ‹ƒAƒhƒŒƒX‚Ìƒ`ƒFƒbƒN
    isMail: function(self) {
        var val = self.val();

        if (val == '') {
            // ‹ó‚Ìê‡‚Í‰½‚à‚µ‚È‚¢
            return true;
        }

        if (val.match(/^[\.a-z0-9!#$%&'*+\/=?^_`{|}~-]+@(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4}|museum|travel)$/i) != null) {
            return true;
        } else {
            return false;
        }
    },

    // “ú•t‚Ìƒ`ƒFƒbƒN
    // “ü—Í‚ª’†“r”¼’[‚Èê‡‚Ínull‚ª‹A‚è‚Ü‚·
    isDate: function(yearId, monthId, dayId) {
        var year = jQuery(yearId, ureru_efo.landingFormObject).val();
        var month = jQuery(monthId, ureru_efo.landingFormObject).val();
        var day = jQuery(dayId, ureru_efo.landingFormObject).val();

        if (year == '' || month == '' || day == '') {
            return null;
        }

        var di = new Date(year, month-1, day);
        if(di.getFullYear() == year && di.getMonth() == (month-1) && di.getDate() == day){
            return true;
        } else {
            return false;
        }
    },

    // 3€–Ú‚ÅŽ©•ªˆÈŠO‚ª“ü—Í‚³‚ê‚Ä‚¢‚é‚©ƒ`ƒFƒbƒN‚·‚é
    // 1€–Ú‚Å‚à“ü—Í‚³‚ê‚Ä‚¢‚½‚ç•K{‚Æ‚·‚é
    isCheckAnother3: function(self, another1, another2) {
        var val1 = self.val();
        var val2 = jQuery(another1, ureru_efo.landingFormObject).val();
        var val3 = jQuery(another2, ureru_efo.landingFormObject).val();

        if (val1 === undefined || val2 === undefined || val3 === undefined) {
            return true;
        }

        if (val1 != '') {
            return true;
        }

        if (val2 != '' || val3 != '') {
            return false;
        } else {
            // ‘S‚Ä‹ó‚Å‚ ‚é
            return true;
        }
    },

    // 3€–Ú‚Å1€–Ú‚Ì“ü—Í˜g‚ªˆê•”‚Ì‚Ý“ü—Í‚É‚È‚Á‚Ä‚È‚¢‚©‚Ìƒ`ƒFƒbƒN
    // ¦”NŒŽ“ú ‚â “d˜b”Ô†‚Ì1`3Œ…
    isCheckPart3: function(key1, key2, key3) {
        var val1 = jQuery(key1, ureru_efo.landingFormObject).val();
        var val2 = jQuery(key2, ureru_efo.landingFormObject).val();
        var val3 = jQuery(key3, ureru_efo.landingFormObject).val();

        if (val1 === undefined || val2 === undefined || val3 === undefined) {
            return true;
        }

        if (val1 != '' && val2 != '' && val3 != '') {
            // ‘S‚Ä‚ª“ü—Í‚³‚ê‚Ä‚¢‚é
            return true;
        } else if(val1 == '' && val2 == '' && val3 == '') {
            // ‘S‚Ä‚ª‹ó‚Å‚ ‚é
            return true;
        } else {
            // ˆê•”“ü—Í‚³‚ê‚Ä‚¢‚é
            return false;
        }
    },

    // “ú•t‚ªˆê•”‚Ì‚Ý“ü—Í‚³‚ê‚Ä‚È‚¢‚©‚Ìƒ`ƒFƒbƒN
    isDatePart: function (yearId, monthId, dayId) {
        var year = jQuery(yearId, ureru_efo.landingFormObject).val();
        var month = jQuery(monthId, ureru_efo.landingFormObject).val();
        var day = jQuery(dayId, ureru_efo.landingFormObject).val();

        if (year != '' && month != '' && day != '') {
            // ‘S‚Ä‚ª“ü—Í‚³‚ê‚Ä‚¢‚é
            return true;
        } else if(year == '' && month == '' && day == '') {
            // ‘S‚Ä‚ª‹ó‚Å‚ ‚é
            return true;
        } else {
            // ˆê•”“ü—Í‚³‚ê‚Ä‚¢‚é
            return false;
        }
    },

    // “d˜b”Ô†‚ªˆê•”‚Ì‚Ý“ü—Í‚³‚ê‚Ä‚È‚¢‚©‚Ìƒ`ƒFƒbƒN
    isTelPart: function (tel1Key, tel2Key, tel3Key) {
        var tel1 = jQuery(tel1Key, ureru_efo.landingFormObject).val();
        var tel2 = jQuery(tel2Key, ureru_efo.landingFormObject).val();
        var tel3 = jQuery(tel3Key, ureru_efo.landingFormObject).val();

        if (tel1 != '' && tel2 != '' && tel3 != '') {
            // ‘S‚Ä‚ª“ü—Í‚³‚ê‚Ä‚¢‚é
            return true;
        } else if(tel1 == '' && tel2 == '' && tel3 == '') {
            // ‘S‚Ä‚ª‹ó‚Å‚ ‚é
            return true;
        } else {
            // ˆê•”“ü—Í‚³‚ê‚Ä‚¢‚é
            return false;
        }
    },

    // ƒtƒH[ƒ€‚Ìƒ^ƒCƒv‚ð•Ô‚·
    getFormType: function(self) {
        var tagName = self.get(0).tagName.toLowerCase(),
            type = '';
        if (tagName == 'input') {
            type = self.attr('type').toLowerCase();
        } else {
            type = tagName;
        }

        return type;
    }
};

function ureru_efo_common_initial(){
    ureru_efo_common.initialize();
}

(function($){
    // ‚³‚Ü‚´‚Ü‚Èform—v‘f‚©‚ç’l‚ðK“¾‚·‚é
    $.fn.multiVal=function(){
        var self = this;
        var tagName = self.get(0).tagName.toLowerCase();
        var val = '';

        if (tagName == 'input') {
            var type = self.attr('type');

            if(type == 'radio' || type == 'checkbox') {
                if (self.filter(":checked").length != 0) {
                    // ƒ`ƒFƒbƒN‚³‚ê‚Ä‚¢‚éê‡‚Ìˆ—(ƒ`ƒFƒbƒNˆ—’Zk‚Ì‚½‚ß‚Éƒ_ƒ~[‚Ì•¶Œ¾‚ðÝ’è)
                    val = self.filter(":checked").val();
                } else {
                    val = '';
                }
            } else {
                val = self.val();
            }
        } else if(tagName == 'select') {
            val = self.children(':selected').val();
        } else if(tagName == 'textarea') {
            val = self.val();
        }

        return val;
    };

})(jQuery);