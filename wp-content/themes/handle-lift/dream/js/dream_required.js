function ureru_required_box_initial(){
    var ureru_required_box = {};
    ureru_required_box.$landingFormWrapper = $('#landing_form-wrapper');

    ureru_required_box.$landingFormWrapper.prepend('<div id="required_box">必須項目が<strong id="efo_required_total">&nbsp;&nbsp;項目</strong>あります。<br />
                                                    <span id="efo_required_remaining">残り<strong>&nbsp;&nbsp;項目</strong></span></div>');

    ureru_required_box.$landingForm = $('#landing_form');
    ureru_required_box.$requiredBox = $('#required_box');
    ureru_required_box.is_webkit = !document.uniqueID && !window.opera && !window.sidebar && window.localStorage && typeof window.orientation == "undefined";
    ureru_required_box.landing_form_position = ureru_required_box.$landingForm.position();
    ureru_required_box.landing_form_width = ureru_required_box.$landingForm.outerWidth();
    ureru_required_box.landing_form_height = ureru_required_box.$landingForm.outerHeight();
    ureru_required_box.required_box_position = ureru_required_box.$requiredBox.position();
    ureru_required_box.required_box_width = ureru_required_box.$requiredBox.outerWidth();
    ureru_required_box.required_box_height = ureru_required_box.$requiredBox.outerHeight();

    ureru_required_box.$requiredBox.css({
        display: 'block',
        visibility: 'hidden'
    });

    ureru_required_box.requiredBoxMarginTop = eval(ureru_required_box.$requiredBox.css('top').replace('px',''));
    ureru_required_box.requiredBoxMarginLeft = eval(ureru_required_box.$requiredBox.css('left').replace('px',''));
    ureru_required_box.requiredBoxWindowMarginTop = eval(ureru_required_box.$requiredBox.css('marginTop').replace('px',''));

    ureru_required_box.$requiredBox.css({
        visibility: '',
        marginTop: 0,
        top: ureru_required_box.landing_form_position.top + ureru_required_box.requiredBoxMarginTop,
        left: ureru_required_box.landing_form_position.left + ureru_required_box.landing_form_width - ureru_required_box.required_box_width + ureru_required_box.requiredBoxMarginLeft
    });

    setRequiredBoxPostion();
    jQuery(window).scroll(setRequiredBoxPostion);

    function setRequiredBoxPostion() {
        var scrollTop = (ureru_required_box.is_webkit ? $('body').scrollTop() : $('html').scrollTop()),
            wrapperTop = ureru_required_box.$landingFormWrapper.position().top + ureru_required_box.requiredBoxMarginTop - ureru_required_box.requiredBoxWindowMarginTop;
        if(scrollTop - (ureru_required_box.landing_form_height - ureru_required_box.required_box_height) > wrapperTop) {
            ureru_required_box.$requiredBox.css('top', (ureru_required_box.landing_form_height - ureru_required_box.required_box_height) + ureru_required_box.requiredBoxMarginTop);
        } else if (scrollTop > wrapperTop) {
            ureru_required_box.$requiredBox.css('top', ureru_required_box.landing_form_position.top + ureru_required_box.requiredBoxMarginTop + scrollTop - wrapperTop);
        } else {
            ureru_required_box.$requiredBox.css('top', ureru_required_box.landing_form_position.top + ureru_required_box.requiredBoxMarginTop);
        }
    }
}
