var ureru_efo = {
    // ‰Šú‰»Ï‚Ýƒtƒ‰ƒO
    isInitialize : false,
    // requiredBoxObject
    requiredBoxObject: null,
    // ƒoƒŠƒf[ƒVƒ‡ƒ“”
    //validCount : 12,
    validCount : 9,

    validList: {
@@@@//"company":false, 
@@@@//"company_kana":false, 
@@@//@"name":false, 
@@@@"familyName":false, 
@@@@"givenName":false, 
@@@@"kana":false, 
@@@@"familyKana":false, 
@@@@"givenKana":false, 
@@@@"tel":false, 
@@@@"telNo1":false, 
@@@@"telNo2":false, 
@@@@"telNo3":false, 
@@@@"email":false, 
@@@@"emailConfirm":false, 
@@@@"zip":false, 
@@@@"zip1":false, 
@@@@"zip2":false, 
@@@@"prefecture":false, 
@@@@"address1":false, 
@@@@"trigger":false, 
@@@@"agreement":false, 
@@@@"mail_optin":true
@@},

    //
    initialize: function() {
        // ƒoƒ‹[ƒ“‰Šú‰»
        ureru_efo.initBalloon();

        //
        ureru_required_box_initial();
        // ƒCƒxƒ“ƒgÝ’è
        ureru_efo.bindEvent();

        // required_box‚ÌƒIƒuƒWƒFƒNƒg‚ðì¬
        ureru_efo.requiredBoxObject = jQuery('#required_box');
        // ‰Šú‰»
        ureru_efo.updateRequiredBox();

        var message = "13€–Ú";
        ureru_efo.requiredBoxObject.children("#required_total").text(message);

        // ‰‰ñ‚ÌƒoƒŠƒf[ƒVƒ‡ƒ“
        if (isPostBack == true) {
            ureru_efo.firstValid();
        }

        // ajaxzip3‚ÌƒŒƒXƒ|ƒ“ƒXƒCƒxƒ“ƒg
        AjaxZip3.onResponse = ureru_efo.onAjaxZip3Response;

        // ©–¼‚©‚ÈŽ©“®“ü—ÍÝ’è
        kntxtext.target = [
            ['data[Contact][family_name]', 'data[Contact][family_kana]', kntxtext.constant.letterType.hira, kntxtext.constant.insertType.auto],
            ['data[Contact][given_name]', 'data[Contact][given_kana]', kntxtext.constant.letterType.hira, kntxtext.constant.insertType.auto]
        ];

        // ‰Šú‰»Ï‚Ý
        ureru_efo.isInitialize = true;
    },

    // ƒoƒ‹[ƒ“‚ð‰Šú‰»‚·‚é
    initBalloon: function() {
        initBalloon("#ContactFamilyName");
        initBalloon("#ContactGivenName");
        initBalloon("#ContactFamilyKana");
        initBalloon("#ContactGivenKana");

        initBalloon("#ContactTelNo1");
        initBalloon("#ContactTelNo2");
        initBalloon("#ContactTelNo3");

        initBalloon("#ContactEmail");
        initBalloon("#ContactEmailConfirm");

        initBalloon("#ContactZip1");
        initBalloon("#ContactZip2");

        initBalloon("#ContactPrefecture");
        initBalloon("#ContactAddress1");

        initBalloon("#ContactTriggerCheckƒZƒ~ƒi[");

        initBalloon("#ContactAgreement2");

        initBalloon("#ContactMailOptinFlg");

        ureru_balloon_initial(false);
    },

    // ƒCƒxƒ“ƒgƒnƒ“ƒhƒ‰‚ðÝ’è
    bindEvent: function() {
        ureru_efo.setEventHandler($("#ContactFamilyName"), ureru_efo.onEntryFamilyName);
        ureru_efo.setEventHandler($("#ContactGivenName"), ureru_efo.onEntryGivenName);
        ureru_efo.setEventHandler($("#ContactFamilyKana"), ureru_efo.onEntryFamilyKana);
        ureru_efo.setEventHandler($("#ContactGivenKana"), ureru_efo.onEntryGivenKana);

        ureru_efo.setEventHandler($("#ContactTelNo1"), ureru_efo.onEntryTelNo1);
        ureru_efo.setEventHandler($("#ContactTelNo2"), ureru_efo.onEntryTelNo2);
        ureru_efo.setEventHandler($("#ContactTelNo3"), ureru_efo.onEntryTelNo3);

        ureru_efo.setEventHandler($("#ContactEmail"), ureru_efo.onEntryEmail);
        ureru_efo.setEventHandler($("#ContactEmailConfirm"), ureru_efo.onEntryEmailConfirm);

        ureru_efo.setEventHandler($("#ContactZip1"), ureru_efo.onEntryZip1);
        ureru_efo.setEventHandler($("#ContactZip2"), ureru_efo.onEntryZip2);

        ureru_efo.setEventHandler($("#ContactPrefecture"), ureru_efo.onPrefecture);
        ureru_efo.setEventHandler($("#ContactAddress1"), ureru_efo.onEntryAddress1);

        ureru_efo.setEventHandler($(".trigger_group input[type=checkbox]"), ureru_efo.onEntryTrigger);

        ureru_efo.setEventHandler($("#ContactAgreement2"), ureru_efo.onEntryAgreement);

        ureru_efo.setEventHandler($("#ContactMailOptinFlg"), ureru_efo.onEntryMailOptin);
    },

    // ‰‰ñƒoƒŠƒf[ƒVƒ‡ƒ“
    firstValid: function() {
        ureru_efo.onEntryUnit('blur');
        ureru_efo.onEntryUnitKana('blur');
        ureru_efo.onEntryFamilyName('blur');
        ureru_efo.onEntryGivenName('blur');
        ureru_efo.onEntryFamilyKana('blur');
        ureru_efo.onEntryGivenKana('blur');
        ureru_efo.onEntryTelNo1('blur');
        ureru_efo.onEntryTelNo2('blur');
        ureru_efo.onEntryTelNo3('blur');
        ureru_efo.onEntryEmail('blur');
        ureru_efo.onEntryEmailConfirm('blur');
        ureru_efo.onEntryZip1('blur');
        ureru_efo.onEntryZip2('blur');
        ureru_efo.onPrefecture('blur');
        ureru_efo.onEntryAddress1('blur');
        ureru_efo.onEntryTrigger('blur');
        ureru_efo.onEntryAgreement('blur');
        ureru_efo.onEntryMailOptin('blur');
    },

    // ƒ`ƒFƒbƒNƒ{ƒbƒNƒX‚Æradioƒ{ƒ^ƒ“‚Ìê‡‚ÍclickƒCƒxƒ“ƒg‚ðÝ’è‚·‚é
    // IEEsafari“™‚ÅblurƒCƒxƒ“ƒg‚ª”­¶‚µ‚È‚¢ˆ×
    setEventHandler: function(obj, handler) {
        if(obj.length == 0) {
            return ;
        }

        var tagName = obj.get(0).tagName.toLowerCase();

        if (tagName == 'input') {
            var type = obj.attr('type');

            if (type == 'checkbox' || type == 'radio') {
                obj.click(handler);
            } else {
                obj.blur(handler);
                obj.keyup(handler);
            }
        } else {
            obj.blur(handler);
            obj.keyup(handler);
        }
    },

    // ‰ïŽÐ–¼
    onEntryCompany: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactCompanyName");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactCompanyName", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["company"] = false;
            ureru_efo.updateRequiredBox();

            ureru_efo.onEntryCompanyKana(eventType);
            return ;
        }

        /*var ret = ureru_efo_common.isZenkakuOnly(self);
        if (ret === false) {
            showBalloonMessage("#ContactCompanyName", "‘SŠp‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["company"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }*/

        hideBalloon("#ContactCompanyName");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["company"] = true;
        ureru_efo.updateRequiredBox();

        ureru_efo.onEntryCompanyKana(eventType);
    },

    // ‰ïŽÐ–¼‚©‚È
    onEntryCompanyKana: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactCompanyKana");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactCompanyKana", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
                self.css({
                    "background-color": "rgb(201, 201, 201)"
                });
            }
            ureru_efo.validList["company_kana"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isHiraganaOnly(self);
        if (ret == false) {
            showBalloonMessage("#ContactCompanyKana", "‚Ð‚ç‚ª‚È‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["company_kana"] = false;
            ureru_efo.updateRequiredBox();
            return;
        }

        hideBalloon("#ContactCompanyKana");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["company_kana"] = true;
        ureru_efo.updateRequiredBox();

        self.css({
            "background-color": "",
            "border": "1px solid rgb(153, 153, 153)"
        });
    },

    // •”–¼
    onEntryUnit: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactDepartmentName");

        var ret = ureru_efo_common.isZenkakuOnly(self);
        if (ret === false) {
            showBalloonMessage("#ContactDepartmentName", "‘SŠp‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            return ;
        }

        hideBalloon("#ContactDepartmentName");
        self.removeClass("efo_input_border_red");

        ureru_efo.updateRequiredBox();

        ureru_efo.onEntryUnitKana(eventType);
    },

    // •”–¼(‚Ó‚è‚ª‚È)
    onEntryUnitKana: function(event) {
        var eventType = ureru_efo.getEventType(event);
        var self  = $("#ContactDepartmentKana");

        if (self.val() == "") {
            self.css({
                    "background-color": "rgb(201, 201, 201)"
                });
            return ;
        }

        var ret = ureru_efo_common.isHiraganaOnly(self);
        if (ret == false) {
            showBalloonMessage("#ContactDepartmentKana", "‚Ð‚ç‚ª‚È‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            return;
        }

        hideBalloon("#ContactDepartmentKana");
        self.removeClass("efo_input_border_red");

        ureru_efo.updateRequiredBox();

        self.css({
            "background-color": "",
            "border": "1px solid rgb(153, 153, 153)"
        });
    },

    // ©
    onEntryFamilyName: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactFamilyName");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactFamilyName", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["familyName"] = false;
            ureru_efo.validList["name"] = false;
            ureru_efo.updateRequiredBox();

            ureru_efo.onEntryFamilyKana(eventType);
            return ;
        }

        var ret = ureru_efo_common.isZenkakuOnly(self);
        if (ret === false) {
            showBalloonMessage("#ContactFamilyName", "‘SŠp‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["familyName"] = false;
            ureru_efo.validList["name"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactFamilyName");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["familyName"] = true;
        if (ureru_efo.validList["givenName"] == true) {
            ureru_efo.validList["name"] = true;
        }
        ureru_efo.updateRequiredBox();

        ureru_efo.onEntryFamilyKana(eventType);
    },

    // –¼
    onEntryGivenName: function (event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactGivenName");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactGivenName", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["givenName"] = false;
            ureru_efo.validList["name"] = false;
            ureru_efo.updateRequiredBox();

            ureru_efo.onEntryGivenKana(eventType);

            return ;
        }

        var ret = ureru_efo_common.isZenkakuOnly(self);
        if (ret === false) {
            showBalloonMessage("#ContactGivenName", "‘SŠp‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["givenName"] = false;
            ureru_efo.validList["name"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactGivenName");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["givenName"] = true;
        if (ureru_efo.validList["familyName"] == true) {
            ureru_efo.validList["name"] = true;
        }
        ureru_efo.updateRequiredBox();

        ureru_efo.onEntryGivenKana(eventType);
    },

    // ‚¹‚¢
    onEntryFamilyKana: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactFamilyKana");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactFamilyKana", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
                self.css({
                    "background-color": "rgb(201, 201, 201)"
                });
            }
            ureru_efo.validList["familyKana"] = false;
            ureru_efo.validList["kana"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isHiraganaOnly(self);
        if (ret == false) {
            showBalloonMessage("#ContactFamilyKana", "w‚¹‚¢x‚Í‚Ð‚ç‚ª‚È‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["familyKana"] = false;
            ureru_efo.validList["kana"] = false;
            ureru_efo.updateRequiredBox();
            return;
        }

        hideBalloon("#ContactFamilyKana");
        self.removeClass("efo_input_border_red");

        // ‚±‚Ìs‚Íajaxzip3‚ÌƒŒƒXƒ|ƒ“ƒX‚ÌŽž‚É“®ì‚·‚é—p
        self.css({
            "background-color": "",
            "border": "1px solid rgb(153, 153, 153)"
        });

        ureru_efo.validList["familyKana"] = true;
        if (ureru_efo.validList["givenKana"] == true) {
            ureru_efo.validList["kana"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // ‚ß‚¢
    onEntryGivenKana: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactGivenKana");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactGivenKana", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
                self.css({
                    "background-color": "rgb(201, 201, 201)"
                });

            }
            ureru_efo.validList["givenKana"] = false;
            ureru_efo.validList["kana"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isHiraganaOnly(self);
        if (ret == false) {
            showBalloonMessage("#ContactGivenKana", "w‚ß‚¢x‚Í‚Ð‚ç‚ª‚È‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["givenKana"] = false;
            ureru_efo.validList["kana"] = false;
            ureru_efo.updateRequiredBox();
            return;
        }

        hideBalloon("#ContactGivenKana");
        self.removeClass("efo_input_border_red");

        // ‚±‚Ìs‚Íajaxzip3‚ÌƒŒƒXƒ|ƒ“ƒX‚ÌŽž‚É“®ì‚·‚é—p
        self.css({
            "background-color": "",
            "border": "1px solid rgb(153, 153, 153)"
        });

        ureru_efo.validList["givenKana"] = true;
        if (ureru_efo.validList["familyKana"] == true) {
            ureru_efo.validList["kana"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // ƒ[ƒ‹ƒAƒhƒŒƒX
    onEntryEmail: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactEmail");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactEmail", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["email"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        if (eventType == "blur" || eventType == "custom") {
            var ret = ureru_efo_common.isMail(self);
            if (ret == false) {
                showBalloonMessage("#ContactEmail", "ƒ[ƒ‹ƒAƒhƒŒƒX‚ª³‚µ‚­‚È‚¢‰Â”\«‚ª‚ ‚è‚Ü‚·");
                self.addClass("efo_input_border_red");
                ureru_efo.validList["email"] = false;
                ureru_efo.updateRequiredBox();
                return ;
            }

            var emailConf = $("#ContactEmailConfirm").val();
            if (emailConf != "") {
                if (selected != emailConf) {
                    showBalloonMessage("#ContactEmail", "Šm”F—pƒ[ƒ‹ƒAƒhƒŒƒX‚Æˆê’v‚µ‚Ä‚Ü‚¹‚ñB");
                    self.addClass("efo_input_border_red");
                    ureru_efo.validList["email"] = false;
                    ureru_efo.updateRequiredBox();
                    return ;
                }
            }
        }

        hideBalloon("#ContactEmail");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["email"] = true;
        ureru_efo.updateRequiredBox();

        if (eventType == "blur") {
            ureru_efo.onEntryEmailConfirm("custom");
        }
    },

    // Šm”F—pƒ[ƒ‹ƒAƒhƒŒƒX
    onEntryEmailConfirm: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactEmailConfirm");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactEmailConfirm", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["emailConfirm"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        if (eventType == "blur" || eventType == "custom") {
            var ret = ureru_efo_common.isMail(self);
            if (ret == false) {
                showBalloonMessage("#ContactEmailConfirm", "ƒ[ƒ‹ƒAƒhƒŒƒXiŠm”Fj‚ª³‚µ‚­‚È‚¢‰Â”\«‚ª‚ ‚è‚Ü‚·");
                self.addClass("efo_input_border_red");
                ureru_efo.validList["emailConfirm"] = false;
                ureru_efo.updateRequiredBox();
                return ;
            }

            var email = $("#ContactEmail").val();
            if (email != "") {
                if (selected != email) {
                    showBalloonMessage("#ContactEmailConfirm", "ƒ[ƒ‹ƒAƒhƒŒƒX‚ªˆê’v‚µ‚Ä‚Ü‚¹‚ñB");
                    self.addClass("efo_input_border_red");
                    ureru_efo.validList["emailConfirm"] = false;
                    ureru_efo.updateRequiredBox();
                    return ;
                }
            }
        }

        hideBalloon("#ContactEmailConfirm");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["emailConfirm"] = true;
        ureru_efo.updateRequiredBox();

        if (eventType == "blur") {
            ureru_efo.onEntryEmail("custom");
        }
    },

    // zip1
    onEntryZip1: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactZip1");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactZip1", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["zip1"] = false;
            ureru_efo.validList["zip"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isNumeric(self);
        if (ret == false) {
            showBalloonMessage("#ContactZip1", "”¼Šp”’l‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["zip1"] = false;
            ureru_efo.validList["zip"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        if (eventType == "blur") {
            var val = self.val();
            if (val.length != 3) {
                showBalloonMessage("#ContactZip1", "—X•Ö”Ô†‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
                ureru_efo.validList["zip1"] = false;
                ureru_efo.validList["zip"] = false;
                ureru_efo.updateRequiredBox();
                return ;
            }
        }

        hideBalloon("#ContactZip1");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["zip1"] = true;
        if (ureru_efo.validList["zip2"] == true) {
            ureru_efo.validList["zip"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // zip2
    onEntryZip2: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactZip2");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactZip2", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["zip2"] = false;
            ureru_efo.validList["zip"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isNumeric(self);
        if (ret == false) {
            showBalloonMessage("#ContactZip2", "”¼Šp”’l‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["zip2"] = false;
            ureru_efo.validList["zip"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        if (eventType == "blur") {
            var val = self.val();
            if (val.length != 4) {
                showBalloonMessage("#ContactZip2", "—X•Ö”Ô†‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
                ureru_efo.validList["zip2"] = false;
                ureru_efo.validList["zip"] = false;
                ureru_efo.updateRequiredBox();
                return ;
            }
        }

        hideBalloon("#ContactZip2");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["zip2"] = true;
        if (ureru_efo.validList["zip1"] == true) {
            ureru_efo.validList["zip"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // ajaxzip3‚ÌƒŒƒXƒ|ƒ“ƒXƒCƒxƒ“ƒg
    onAjaxZip3Response: function () {
        ureru_efo.onPrefecture("blur");
        ureru_efo.onEntryAddress1("blur");
    },

    // “s“¹•{Œ§
    onPrefecture: function (event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactPrefecture");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactPrefecture", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["prefecture"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactPrefecture");
        self.removeClass("efo_input_border_red");

        // ‚±‚Ìs‚Íajaxzip3‚ÌƒŒƒXƒ|ƒ“ƒX‚ÌŽž‚É“®ì‚·‚é—p
        self.css({
            "background-color": "",
            "border": "1px solid rgb(153, 153, 153)"
        });

        ureru_efo.validList["prefecture"] = true;
        ureru_efo.updateRequiredBox();
    },

    // ZŠ1
    onEntryAddress1: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactAddress1");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactAddress1", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["address1"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactAddress1");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["address1"] = true;
        ureru_efo.updateRequiredBox();
    },

    // “d˜b”Ô†1
    onEntryTelNo1: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactTelNo1");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactTelNo1", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["telNo1"] = false;
            ureru_efo.validList["tel"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isNumeric(self);
        if (ret == false) {
            showBalloonMessage("#ContactTelNo1", "”¼Šp”’l‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["telNo1"] = false;
            ureru_efo.validList["tel"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactTelNo1");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["telNo1"] = true;
        if (ureru_efo.validList["telNo2"] == true && ureru_efo.validList["telNo3"] == true) {
            ureru_efo.validList["tel"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // “d˜b”Ô†2
    onEntryTelNo2: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactTelNo2");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactTelNo2", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["telNo2"] = false;
            ureru_efo.validList["tel"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isNumeric(self);
        if (ret == false) {
            showBalloonMessage("#ContactTelNo2", "”¼Šp”’l‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["telNo2"] = false;
            ureru_efo.validList["tel"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactTelNo2");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["telNo2"] = true;
        if (ureru_efo.validList["telNo1"] == true && ureru_efo.validList["telNo3"] == true) {
            ureru_efo.validList["tel"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // “d˜b”Ô†3
    onEntryTelNo3: function(event) {
        var eventType = ureru_efo.getEventType(event);

        var self  = $("#ContactTelNo3");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#ContactTelNo3", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            ureru_efo.validList["telNo3"] = false;
            ureru_efo.validList["tel"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        var ret = ureru_efo_common.isNumeric(self);
        if (ret == false) {
            showBalloonMessage("#ContactTelNo3", "”¼Šp”’l‚ð“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["telNo3"] = false;
            ureru_efo.validList["tel"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactTelNo3");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["telNo3"] = true;
        if (ureru_efo.validList["telNo1"] == true && ureru_efo.validList["telNo2"] == true) {
            ureru_efo.validList["tel"] = true;
        }
        ureru_efo.updateRequiredBox();
    },

    // ‚Ç‚±‚Å’m‚Á‚½‚©H
    onEntryTrigger: function() {
        var self  = $(".trigger_group input[type=checkbox]");

        var checkedFlg = false;
        self.each(function() {
            var checked = $(this).is(":checked");
            if (checked == true) {
                checkedFlg = true;
            }
        });

        if (checkedFlg == false) {
            showBalloonMessage("#ContactTriggerCheckƒZƒ~ƒi[", "ƒ`ƒFƒbƒN‚ð“ü‚ê‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["trigger"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactTriggerCheckƒZƒ~ƒi[");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["trigger"] = true;
        ureru_efo.updateRequiredBox();
    },

    // “¯ˆÓ
    onEntryAgreement: function() {
        var self  = $("#ContactAgreement2");
        var selected = self.is(':checked');
        if (selected == false) {
            showBalloonMessage("#ContactAgreement2", "ŒÂlî•ñ‹K–ñ“¯ˆÓ‚Éƒ`ƒFƒbƒN‚ð“ü‚ê‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["agreement"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactAgreement2");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["agreement"] = true;
        ureru_efo.updateRequiredBox();
    },

    // ƒIƒvƒgƒCƒ“
    onEntryMailOptin: function() {
        var self  = $("#ContactMailOptinFlg");
        var selected = self.is(':checked');
        if (selected == false) {
            showBalloonMessage("#ContactMailOptinFlg", "V’…î•ñ‚ðŽó‚¯Žæ‚é‚Éƒ`ƒFƒbƒN‚ð“ü‚ê‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            ureru_efo.validList["mail_optin"] = false;
            ureru_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#ContactMailOptinFlg");
        self.removeClass("efo_input_border_red");

        ureru_efo.validList["mail_optin"] = true;
        ureru_efo.updateRequiredBox();
    },

    // •K{€–Ú”‚ðƒ`ƒFƒbƒN‚·‚é
    updateRequiredBox: function() {
        var count = 0;
        for (k in ureru_efo.validList) {
            if (k == "familyKana" || k == "familyName" || k == "givenKana" || k == "givenName" || k == "telNo1" || k == "telNo2" || k == "telNo3" || k == "zip1" || k == "zip2") {
                continue;
            }

            if (ureru_efo.validList[k] == false) {
                count++;
            }
        }

        if (count > 0) {
            ureru_efo.requiredBoxObject.removeClass('complete');
            message = "Žc‚è<strong>" + count + "€–Ú</strong>";

            // ƒ{ƒ^ƒ“‚ð–¢“ü—Í‚ ‚è‚É•ÏX(Ô)
            $("#confirm_button_disable").show();
            $("#confirm_button").hide();
        } else {
            ureru_efo.requiredBoxObject.addClass('complete');
            message = "‘S‚Ä“ü—Í‚³‚ê‚Ä‚¢‚Ü‚·B";

            // ƒ{ƒ^ƒ“‚ð“ü—ÍŠ®—¹‚É•ÏX(—Î)
            $("#confirm_button_disable").hide();
            $("#confirm_button").show();
        }

        jQuery("#efo_required_remaining").html(message);
    },

    // ƒCƒxƒ“ƒgŽí•Ê‚ðŽæ“¾‚·‚é
    getEventType: function(event) {
        if (typeof event == "string") {
            return event;
        } else {
            return event.type;
        }
    },

    dummy: function() {
    }
}
$(function() {
    ureru_efo.initialize();
});
