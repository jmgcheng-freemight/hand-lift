function ureru_balloon_initial() {
    var script_tag     = document.createElement("script");
    script_tag.type    = "text/javascript";
    script_tag.charset = "utf-8";
    script_tag.src     = global.baseUrl + "js/jquery.tipsy.js?" + global.cache_clear_string;
    $("body").append(script_tag);

    // ƒoƒ‹[ƒ“‚ðƒNƒŠƒbƒN‚ÅÁ‹Ž
    $(".tipsy").on('click', function () {
            var tipId = jQuery(this).attr('id');
            tipId = tipId.replace('tip-', '');
            jQuery("#" + tipId).tipsy('hide');
        });
}

// ƒoƒ‹[ƒ“‚Ì‰Šú‰»
function initBalloon(obj) {
    if(jQuery(obj).length == 0) {
        return false;
    }

    // ƒoƒ‹[ƒ“‚É•t‰Á‚·‚éƒNƒ‰ƒX–¼
    var balloonClassName = 'tip_' + obj.replace(/^\.|^#/, '');

    jQuery(obj).attr('original-title', '');
    jQuery(obj).tipsy({className: balloonClassName});
}

function setBalloonMessage(obj, message) {
    jQuery(obj).attr('original-title', message);
}

function showBalloonMessage(obj, message) {
    if(message) {
        var balloonClassName = '.tip_' + obj.replace(/^\.|^#/, '');

        // @Todo : ‚±‚Ì‰º‚Ìif‚ª‚È‚¢‚Æƒ`ƒJƒ`ƒJ‚·‚é‚ªAƒtƒF[ƒhƒAƒEƒg‚µ‚Ä‚éŽž‚É’Ç‰Á‚Å‚«‚È‚¢
        //if(jQuery(balloonClassName).length == 0) {
            jQuery(obj).attr('original-title', message);
            jQuery(obj).tipsy('show');
        //}
    } else {
        jQuery(obj).tipsy('hide');
    }
}

function hideBalloon(obj) {
    jQuery(obj).tipsy('hide');
}
