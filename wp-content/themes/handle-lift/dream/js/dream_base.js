var dream_efo = {
    // ‰Šú‰»Ï‚Ýƒtƒ‰ƒO
    isInitialize : false,
    // requiredBoxObject
    requiredBoxObject: null,
    // ƒoƒŠƒf[ƒVƒ‡ƒ“”
    //validCount : 12,
    validCount : 9,

    validList: {
@@@@"name1":false, 
@@@@"name2":false, 
@@@@"kana1":false, 
@@@@"kana2":false, 
@@@@"zip":false, 
@@@@"pref":false, 
@@@@"address":false, 
@@@@"tel":false, 
@@@@"email":false
@@},
    initialize: function() {
        required_box_initial();
        // ƒCƒxƒ“ƒgÝ’è
        dream_efo.bindEvent();

        // required_box‚ÌƒIƒuƒWƒFƒNƒg‚ðì¬
        dream_efo.requiredBoxObject = jQuery('#required_box');
        // ‰Šú‰»
        dream_efo.updateRequiredBox();

        var message = "9€–Ú";
        dream_efo.requiredBoxObject.children("#required_total").text(message);

        // ‰‰ñ‚ÌƒoƒŠƒf[ƒVƒ‡ƒ“
        if (isPostBack == true) {
            dream_efo.firstValid();
        }
        // ‰Šú‰»Ï‚Ý
        dream_efo.isInitialize = true;
    },
    // ƒCƒxƒ“ƒgƒnƒ“ƒhƒ‰‚ðÝ’è
    bindEvent: function() {
        dream_efo.setEventHandler($("#sei"), dream_efo.onEntrySei);
        dream_efo.setEventHandler($("#mei"), dream_efo.onEntryMei);
        dream_efo.setEventHandler($("#sei-kana"), dream_efo.onEntrySeiKana);
        dream_efo.setEventHandler($("#mei-kana"), dream_efo.onEntryMeiKana);

        dream_efo.setEventHandler($("#zipcode"), dream_efo.onEntryZip);
        dream_efo.setEventHandler($("#prefecture"), dream_efo.onEntryPrefecture);
        dream_efo.setEventHandler($("#address"), dream_efo.onEntryAddress);
        dream_efo.setEventHandler($("#telephone"), dream_efo.onEntryTelephone);
        dream_efo.setEventHandler($("#email"), dream_efo.onEntryEmail);
    },

    // ‰‰ñƒoƒŠƒf[ƒVƒ‡ƒ“
    firstValid: function() {
        dream_efo.onEntryUnit('blur');
        dream_efo.onEntryUnitKana('blur');
        dream_efo.onEntrySei('blur');
        dream_efo.onEntryMei('blur');
        dream_efo.onEntrySeiKana('blur');
        dream_efo.onEntryMeiKana('blur');
        dream_efo.onEntryZip('blur');
        dream_efo.onEntryPrefecture('blur');
        dream_efo.onEntryAddress('blur');
        dream_efo.onEntryTelephone('blur');
        dream_efo.onEntryEmail('blur');
    },

    // ©
    onEntrySei: function(event) {
        var eventType = dream_efo.getEventType(event);

        var self  = $("#sei");
        var selected = self.val();
        if (selected == "") {
            if (eventType == 'blur') {
                showBalloonMessage("#sei", "“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
                self.addClass("efo_input_border_red");
            }
            dream_efo.validList["sei"] = false;
            //ureru_efo.validList["name"] = false;
            dream_efo.updateRequiredBox();

            dream_efo.onEntrySei(eventType);
            return ;
        },

        var ret = dream_efo_common.isZenkakuOnly(self);
        if (ret === false) {
            showBalloonMessage("#sei", "‘SŠp‚Ì‚Ý‚Å“ü—Í‚µ‚Ä‚­‚¾‚³‚¢");
            self.addClass("efo_input_border_red");
            dream_efo.validList["sei"] = false;
            //dream_efo.validList["name"] = false;
            dream_efo.updateRequiredBox();
            return ;
        }

        hideBalloon("#sei");
        self.removeClass("efo_input_border_red");

        dream_efo.validList["sei"] = true;
        if (dream_efo.validList["sei"] == true) {
            //dream_efo.validList["name"] = true;
        }
        dream_efo.updateRequiredBox();

        dream_efo.onEntrySeiKana(eventType);
    },


    // •K{€–Ú”‚ðƒ`ƒFƒbƒN‚·‚é
    updateRequiredBox: function() {
        var count = 0;
        for (k in dream_efo.validList) {
            if (k == "name1" || k == "name2" || k == "kana1" || k == "kana2" || k == "zip" || k == "pref" || k == "address" || k == "tel" || k == "email") {
                continue;
            }

            if (dream_efo.validList[k] == false) {
                count++;
            }
        }

        if (count > 0) {
            dream_efo.requiredBoxObject.removeClass('complete');
            message = "Žc‚è<strong>" + count + "€–Ú</strong>";

            // ƒ{ƒ^ƒ“‚ð–¢“ü—Í‚ ‚è‚É•ÏX(Ô)
            $("#confirm_button_disable").show();
            $("#confirm_button").hide();
        } else {
            dream_efo.requiredBoxObject.addClass('complete');
            message = "‘S‚Ä“ü—Í‚³‚ê‚Ä‚¢‚Ü‚·B";

            // ƒ{ƒ^ƒ“‚ð“ü—ÍŠ®—¹‚É•ÏX(—Î)
            $("#confirm_button_disable").hide();
            $("#confirm_button").show();
        }

        jQuery("#required_remaining").html(message);
    },

    // ƒCƒxƒ“ƒgŽí•Ê‚ðŽæ“¾‚·‚é
    getEventType: function(event) {
        if (typeof event == "string") {
            return event;
        } else {
            return event.type;
        }
    },

    dummy: function() {
    }
}

$(function() {
    dream_efo.initialize();
});