<?php 
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
		
		<script src="<?php echo get_template_directory_uri();?>/dream/js/jquery-1.11.2.js" type="text/javascript"></script>

		<!-- if order template -->
		<?php if( is_order_form($post->ID) ): ?>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/common/css/import.css" type="text/css" />
			
			<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/common/css/animate.css" type="text/css" />
			<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/common/css/order.css" type="text/css" />
			<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/dream/css/dream_required_box.css" type="text/css" />
			<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/dream/jQuery-Validation/css/validationEngine.jquery.css" type="text/css" />
			
			<script src="<?php echo get_template_directory_uri();?>/common/js/common.js" type="text/javascript" ></script>
			<script src="<?php echo get_template_directory_uri();?>/js/jquery-modal-master/jquery.modal.min.js" type="text/javascript" charset="utf-8"></script>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/jquery-modal-master/jquery.modal.css" type="text/css" media="screen" />
		<?php endif; ?>
		<!--end of if order template -->
		
		<script type="text/JavaScript">
        <!--
		function MM_preloadImages() { //v3.0
		  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}

		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

		function MM_findObj(n, d) { //v4.01
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		}

		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}
		//-->
		</script>
		
		<?php if($_SERVER['HTTP_HOST'] == 'hl.dev'): ?>
			<script type="text/javascript">
				var ajaxurl = '/wp-admin/admin-ajax.php';
			</script>
		<?php else:?>
			<script type="text/javascript">
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';
			</script>
		<?php endif;?>
		<?php wp_head(); ?>

		<title>
			<?php 
				if ( is_front_page() )
				{
					bloginfo('name');
					/*echo 'ご利用ガイド｜ハンドリフト　ハンドパレット.com 全国対応のハンドリフト専門店';*/
				}
				elseif( is_archive() )
				{
					$s_slug = get_post_type();
					if( isset($s_slug) && !empty($s_slug) )
					{
						$a_args = array(
							'post_type' => 'page',
							'pagename' => $s_slug
						);
						$o_custom_query = new WP_Query( $a_args );
						if ( $o_custom_query->have_posts() ) 
						{
								$o_custom_query->the_post();
								echo get_the_title();
						}
						wp_reset_postdata();
					}
					else
					{
						wp_title('', true);			
					}
				}
				else
				{
					wp_title('', true);	
				}
			?>
		</title>
		<!-- if order template -->
		<?php if( is_order_form($post->ID) )echo order_style(); ?>
		
	</head>
	<body>


		<div id="js-container-whole" class="">



			<header class="l-header">

				<div class="l-content">
					<div class="l-gutter">

						<div class="l-table width-full">
							<div class="l-table-cell">
								<a class="header-logo" href="<?php bloginfo ('url'); ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-1.jpg" />
								</a>
							</div>
							<div class="l-table-cell align-right align-middle">
								<div class="header-bannercontact display-pc">
									<p class="header-bannercontact-msg">
										お電話でのお問い合わせ・ご注文
									</p>
									<p class="header-bannercontact-num">
										0120-019-522
									</p>
									<p class="header-bannercontact-time">
										平日10：00ー18：00
									</p>
								</div>

								<nav class="nav-header nav-header-slide display-sp">
									<div id="js-nav-trigger" class="nav-trigger">
										<div class="nav-trigger-bar nav-trigger-bar-top"></div>
										<div class="nav-trigger-bar nav-trigger-bar-middle"></div>
										<div class="nav-trigger-bar nav-trigger-bar-bottom"></div>
									</div>
								</nav>

							</div>
						</div>

					</div>
				</div>


				<nav class="nav nav-header nav-header-pc display-pc">
					<div class="l-content">
						<div class="l-gutter">
							<ul class="l-grid l-grid-5cols">
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>">ホーム</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/reason">当店が選ばれる理由</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/announce">格安の理由</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/hosyo">安心保障について</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/staff">スタッフ紹介</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>


				<nav class="nav nav-header nav-header-sp display-sp ">
					<div class="l-content">
						<div class="l-gutter">
							<ul class="l-grid l-grid-4cols">
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/reason">当店が選ばれる理由</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/announce">格安の理由</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/hosyo">安心保障について</a>
								</li>
								<li class="l-grid-col">
									<a href="<?php bloginfo ('url'); ?>/staff">スタッフ紹介</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>


			</header>


		    <div class="wrapper cf">

		    	<div class="l-content">
					<div class="l-gutter">

				        <!--+++.header+++-->
				        <!-- <div class="header cf">
				            <h1><a href="<?php bloginfo ('url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/header.jpg" alt="全国対応のハンドリフト専門店 ハンドリフト、ハンドパレット、ハンドトラック.com" /></a></h1>
				            <ul>
				                <li id="gnavi01">
				                    <a href="<?php bloginfo ('url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/gnavi01.jpg" alt="トップページ" />
				                    </a>
				                </li>
				                <li id="gnavi02">
				                    <a href="<?php bloginfo ('url'); ?>/reason"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/gnavi06.jpg" alt="当店が選ばれる理由" />
				                    </a>
				                </li>
				                <li id="gnavi03">
				                    <a href="<?php bloginfo ('url'); ?>/announce"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/gnavi03.jpg" alt="格安の理由" />
				                    </a>
				                </li>
				                <li id="gnavi04">
				                    <a href="<?php bloginfo ('url'); ?>/hosyo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/gnavi07.jpg" alt="安心保障について" />
				                    </a>
				                </li>
				                <li id="gnavi05">
				                    <a href="<?php bloginfo ('url'); ?>/staff"><img src="<?php echo get_stylesheet_directory_uri(); ?>/common/img/gnavi08.jpg" alt="スタッフ紹介" />
				                    </a>
				                </li>
				            </ul>
				        </div> -->
				        <!--+++/.header+++-->

				        <?php if ( is_front_page() ) : ?>
				        <!--+++.mainimage/+++-->
				        <!-- <div class="mainimage">
				            <h2> 
				            	<img src="<?php echo get_stylesheet_directory_uri(); ?>/top/mainimage.jpg" alt="使いやすく、壊れないハンドリフトを業界最安値でご提供します。" />
				      		</h2>
				        </div> -->
				        <!--+++/.mainimage+++-->
				        <?php endif ; ?>