<?php
/**
 * Handle Lift functions and definitions
 *
 */

/* update_option('siteurl','hl.dev'); 
update_option('home','hl.dev');
 */
function handlelift_setup() {

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	/*add_theme_support( 'title-tag' );*/


	/*
		create product post type
	*/
	$a_labels = array(
		'name'                => _x( 'Products', 'Post Type General Name' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Products' ),
		'parent_item_colon'   => __( 'Parent Product' ),
		'all_items'           => __( 'All Products' ),
		'view_item'           => __( 'View Product' ),
		'add_new_item'        => __( 'Add New Product' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Product' ),
		'update_item'         => __( 'Update Product' ),
		'search_items'        => __( 'Search Product' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'Products' ),
		'description'         => __( 'Products' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'products', $a_args );


	/*
		create useguide post type
	*/
	$a_labels = array(
		'name'                => _x( 'Useguides', 'Post Type General Name' ),
		'singular_name'       => _x( 'Useguide', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Useguides' ),
		'parent_item_colon'   => __( 'Parent Useguide' ),
		'all_items'           => __( 'All Useguides' ),
		'view_item'           => __( 'View Useguide' ),
		'add_new_item'        => __( 'Add New Useguide' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Useguide' ),
		'update_item'         => __( 'Update Useguide' ),
		'search_items'        => __( 'Search Useguide' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'Useguides' ),
		'description'         => __( 'Useguides' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'useguide', $a_args );


	/*
		create handlift post type
	*/
	$a_labels = array(
		'name'                => _x( 'User Datas', 'Post Type General Name' ),
		'singular_name'       => _x( 'User Data', 'Post Type Singular Name' ),
		'menu_name'           => __( 'User Datas' ),
		'parent_item_colon'   => __( 'Parent User Data' ),
		'all_items'           => __( 'All User Datas' ),
		'view_item'           => __( 'View User Data' ),
		'add_new_item'        => __( 'Add New User Data' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit User Data' ),
		'update_item'         => __( 'Update User Data' ),
		'search_items'        => __( 'Search User Data' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'User Datas' ),
		'description'         => __( 'User Datas' ),
		'labels'              => $a_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'user-data', $a_args ); //old site use user_data, just following that..
	
	/*
		create handlift orders post type jb*
	*/
	$o_labels = array(
		'name'                => _x( 'Orders', 'Post Type General Name' ),
		'singular_name'       => _x( 'Order', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Orders' ),
		'parent_item_colon'   => __( 'Parent Orders' ),
		'all_items'           => __( 'All Orders' ),
		'view_item'           => __( 'View Orders' ),
		'add_new_item'        => __( 'Add New Orders' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Orders' ),
		'update_item'         => __( 'Update Orders' ),
		'search_items'        => __( 'Search Orders' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$o_args = array(
		'label'               => __( 'Orders' ),
		'description'         => __( 'Orders' ),
		'labels'              => $o_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => false,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		/*'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),*/
		'menu_position'      => 5
	);
	register_post_type( 'orders', $o_args ); //old site use user_data, just following that..

}
add_action( 'after_setup_theme', 'handlelift_setup' );

	
/*function filter_function_name($title) {
	return $title;
}
add_filter( 'wp_title', 'filter_function_name', 10, 2 );*/

function wpdocs_enqueue_custom_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/common/css/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );

// Add metabox for producst 
add_action( 'save_post', 'product_meta_box_save' );
add_action( 'add_meta_boxes', 'product_meta_box_add' ); 

function product_meta_box_add(){
        add_meta_box( 'predefined_field', 'Product Details', 'product_meta_box_html', 'products', 'side', '' );
}

function product_meta_box_save( $post_id ){

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'product_meta_box_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
	
	$product_number_value = isset($_POST['product_number_field']) ? $_POST['product_number_field'] : '';
	$product_name_field = isset($_POST['product_name_field']) ? $_POST['product_name_field'] : '';
	$load_capacity_value = isset($_POST['load_capacity_field']) ? $_POST['load_capacity_field'] : '';
	$max_height_value = isset($_POST['max_height_field']) ? $_POST['max_height_field'] : '';
	$min_height_value = isset($_POST['min_height_field']) ? $_POST['min_height_field'] : '';
	$weight_value = isset($_POST['weight_field']) ? $_POST['weight_field'] : '';
	$shipping_value = isset($_POST['shipping_field']) ? $_POST['shipping_field'] : '';
	$original_price_value = isset($_POST['original_price_field']) ? $_POST['original_price_field'] : '';
	$comment_value = isset($_POST['comment_field']) ? $_POST['comment_field'] : '';
	$limited_price_value = isset($_POST['limited_price_field']) ? $_POST['limited_price_field'] : '';
	$av_stock_value = isset($_POST['av_stock_field']) ? $_POST['av_stock_field'] : '';
	
	$product_number_value = sanitize_text_field($product_number_value);
	$product_name_field = sanitize_text_field($product_name_field);
	$load_capacity_value = sanitize_text_field($load_capacity_value); 
	$max_height_value = sanitize_text_field($max_height_value);
	$min_height_value = sanitize_text_field($min_height_value);
	$weight_value = sanitize_text_field($weight_value);
	$shipping_value = sanitize_text_field($shipping_value);
	$original_price_value = sanitize_text_field($original_price_value);
	$comment_value = sanitize_text_field($comment_value);
	$limited_price_value = sanitize_text_field($limited_price_value);
	$av_stock_value = sanitize_text_field($av_stock_value);
	
    if( $product_number_value || $load_capacity_value || $max_height_value ){
		update_post_meta($post_id,'product_number',$product_number_value);
		update_post_meta($post_id,'product_name',$product_name_field);
		update_post_meta($post_id,'load_capacity',$load_capacity_value);
		update_post_meta($post_id,'max_height',$max_height_value);
		update_post_meta($post_id,'min_height',$min_height_value);
		update_post_meta($post_id,'weight',$weight_value);
		update_post_meta($post_id,'shipping',$shipping_value);
		update_post_meta($post_id,'original_price',$original_price_value);
		update_post_meta($post_id,'comment',$comment_value);
		update_post_meta($post_id,'limited_price',$limited_price_value);
		update_post_meta($post_id,'av_stock',$av_stock_value);
	}
	
}

function product_meta_box_html( $post ){
    wp_nonce_field( 'product_meta_box_nonce', 'meta_box_nonce' );
	
    //if you know it is not an array, use true as the third parameter
    $product_number_value = get_post_meta($post->ID,'product_number',true);
    $product_name_field = get_post_meta($post->ID,'product_name',true);
    $load_capacity_value = get_post_meta($post->ID,'load_capacity',true);
    $max_height_value = get_post_meta($post->ID,'max_height',true);
    $min_height_value = get_post_meta($post->ID,'min_height',true);
    $weight_value = get_post_meta($post->ID,'weight',true);
    $shipping_value = get_post_meta($post->ID,'shipping',true);
    $original_price_value = get_post_meta($post->ID,'original_price',true);
    $limited_price_value = get_post_meta($post->ID,'limited_price',true);
    $av_stock_value = get_post_meta($post->ID,'av_stock',true);
    $sold_units = get_post_meta($post->ID,'sold_units',true);
    $comment_value = get_post_meta($post->ID,'comment',true);

    ?>
		<label class="meta-label" for="product_number_field">商品番号</label><input name="product_number_field" id="product_number_field" type="text"  value="<?php echo $product_number_value; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="product_name_field">注文商品名</label><input name="product_name_field" id="product_number_field" type="text"  value="<?php echo $product_name_field; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="load_capacity_field">耐荷重</label><input name="load_capacity_field" id="load_capacity_field" type="text"  value="<?php echo $load_capacity_value; ?>" class="mws-textinput" /><span>kg</span><br/>
		<label class="meta-label" for="max_height_field">最高位</label><input name="max_height_field" id="max_height_field" type="text"  value="<?php echo $max_height_value; ?>" class="mws-textinput" /><span>mm</span><br/>
		<label class="meta-label" for="min_height_field">最低位</label><input name="min_height_field" id="min_height_field" type="text"  value="<?php echo $min_height_value; ?>" class="mws-textinput" /><span>mm</span><br/>
		<label class="meta-label" for="weight_field">重　量</label><input name="weight_field" id="weight_field" type="text"  value="<?php echo $weight_value; ?>" class="mws-textinput" /><span>kg</span><br/>
		<label class="meta-label" for="shipping_field">送　料</label><input name="shipping_field" id="shipping_field" type="text"  value="<?php echo $shipping_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="original_price_field">通常価格</label><input name="original_price_field" id="original_price_field" type="text"  value="<?php echo $original_price_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="limited_price_field">限定価格</label><input name="limited_price_field" id="limited_price_field" type="text"  value="<?php echo $limited_price_value; ?>" class="mws-textinput" /><span>円</span><br/>
		<label class="meta-label" for="av_stock_field">在庫</label><input name="av_stock_field" id="av_stock_field" type="text"  value="<?php echo $av_stock_value; ?>" class="mws-textinput" /><br/>
		<label class="meta-label" for="av_stock_field">個数: </label><span><?php echo $sold_units;?></span><br>
		<label class="meta-label" for="comment_field">限定価格</label><textarea name="comment_field" id="comment_field" class="mws-textinput" /><?php echo $comment_value; ?></textarea><br/>
		
	<?php	

}


//custom functions 
include_once( get_stylesheet_directory() . '/includes/custom-function.php' );